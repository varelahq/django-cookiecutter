from django.conf.urls import url

from . import views

app_name = '{{cookiecutter.project_slug}}core'

urlpatterns = [
    url(r'^$', views.index, name='index'),
]
