from django.shortcuts import render


def index(request):
    template = '{{cookiecutter.project_slug}}core/index.html'
    context = {}
    return render(request, template_name=template, context=context)
