from django.apps import AppConfig


class {{cookiecutter.project_slug}}coreConfig(AppConfig):
    name = '{{cookiecutter.project_slug}}core'
